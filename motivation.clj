;Summary: A mammalian brain will not be motivated to act unless it believes that it will see the rewards of the expected actions over the expected work period.

;Motivation = E(Work*Rewards)/Time

;The brain extrapolates expectation from past experience. It is not evolutionarily efficient to hold all past experiences equal. The older an experience is the less bearing it has on the present moment. Let's play with an exponential function using a week as the total period. 
                               
(def number-e 2.71828) 

(for [i (range 1 7)] ;Plotting weights with forgetting curve
  (let [day-weight (/ (/ 100 i) number-e)]
    (println day-weight))) 

