(ns casinowar.core-test
  (:require [clojure.test :refer :all]
            [casinowar.core :refer :all]))

(deftest deck-test
    "Checks if make-decks generates a vector of vectors."
    (is (type (make-decks 6)) clojure.lang.PersistentVector))
