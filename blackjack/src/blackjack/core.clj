(ns blackjack.core
  (:require [cheshire.core :refer :all]
            [clojure.data.json :as json])
  (:gen-class)) 

(def random-seed (json/read-str (slurp "seed/gull.json")
                                :key-fn keyword)) 

(defn fetch-seed []
  ((random-seed :array) (rand-int (count (random-seed :array))))) 

(defn shuffle-with-seed
    "Return a random permutation of coll with a seed"
    [coll seed]
    (let [al (java.util.ArrayList. coll)
                  rnd (java.util.Random. seed)]
          (java.util.Collections/shuffle al rnd)
          (clojure.lang.RT/vector (.toArray al)))) 

(defn make-decks
  "Returns n decks of cards for every game."
  [n]
  (vec (shuffle-with-seed 
         (take (* n 52) (cycle (for [i ["♠" "♦" "♣" "♥"] ;"♤" "♡" "♢" "♧"
                                     j (flatten (conj (range 2 12)
                                                      (take 3 (repeat 10))))]
    (do [i j])))) (fetch-seed)))) 

(defn return-integers
  "Takes a mixed collection and returns a vector of its integers."
  [mixed-collection]
  (vec (filter number? (flatten mixed-collection)))) 

(defn until
  "Returns the deck of possible outcomes, i.e. up until n"
  [n deck]
  (let [sum (atom 0)
        counter (atom 1)
        array (return-integers deck)]
    (while (< @sum n)
        (swap! counter inc)
        (do
            (reset! sum (reduce
                         + (take @counter array)))))
  (take @counter deck))) 

(defn count-points [deck]
  (reduce + (return-integers deck))) 

(defn find-n-point [v n]
      "Index of the number which stands
     where the sum up until then is over 21."
      (count (filter #(<= % n)
                                 (reductions + v)))) 

(defn has-element?
  [el vect]
  "Returns true if vector contains element"
    (= (some #{el} vect) el))  

(defn partial-at-point [n-point v]
  (vec (take (inc n-point) v))) 

(defn change-ace-value [v max]
  (let [point-max (find-n-point v max)
        partial-v (partial-at-point point-max v)
        index-of-11 (.indexOf partial-v 11)]
    (if (and (> (reduce + partial-v) 21)
             (not= index-of-11 -1))
      (recur (vec
              (concat (assoc partial-v index-of-11 1)
                      (drop (inc point-max) v))) max)
            partial-v))) 

(defn double-ace?
  "Returns true if two aces in deck"
  [array]
  (= 2 (count (filter true? (for [i array]
                                  (has-element? 11 i)))))) 

(def deck (make-decks 6)) 

(defn over-21?
  "Returns true if sum of vector elements
  is over 21"
    [v]
    (< 21 (reduce + (filter number?
                            (flatten v))))) 

(defn gen-dealer-deck [max]
  "Hits dealer until sum is less than max."
  (let [card-integers (change-ace-value (return-integers
                                         (take-nth 2 deck)) (dec max))
        card-strings (filter string?
                             (flatten (take (count card-integers)
                                            (take-nth 2 deck))))]
    (apply map vector [card-strings
                       card-integers]))) 

(let [basic-deck (gen-dealer-deck 17)
      soft-17-deck (gen-dealer-deck 18)]
  (def dealer-deck
    (if (and
         (= (count basic-deck) 3)
         (<= (count-points basic-deck) 17))
    soft-17-deck
    basic-deck))) 

(defn make-player-deck [n]
  (until n
   (take-nth 2 (rest deck)))) 

(def player-deck (vec (make-player-deck 62))) 

(defn first-two-ace? [p k]
  (let [p (first p)
        k (first k)]
    (if (and (= p 1)
             (= k 11))
      true
      (if (and (= p 11)
               (= k 1))
        true
        false))))

(defn split-deck [deck n]
  (let [first-card (return-integers (first deck))
        second-card (return-integers (second deck))]
    (if (or (= first-card second-card)
            (first-two-ace? first-card second-card))
      (case n :one
            (take-nth 2 (make-player-deck 62))
            n :two
            (take-nth 2 (rest (make-player-deck 62))))))) 

(defn sum-seq [deck]
  "Returns integer reductions for deck"
  (if (not= (count (first deck)) 2)
    (for [i deck]
      (reductions + (return-integers i)))
    (reductions + (return-integers deck)))) 

(defn generate-results []
  {:dealer-deck (into (vec dealer-deck) [(sum-seq dealer-deck)])
   :player-deck (into (vec player-deck) [(sum-seq player-deck)])
   :split-deck-one (into (vec (split-deck player-deck :one)) [(sum-seq (split-deck player-deck :one))])
   :split-deck-two (into (vec (split-deck player-deck :two)) [(sum-seq (split-deck player-deck :two))])})

(defn -main
    "Casino War"
   [& args]
   (println
    (generate-string
     (generate-results))))
