(def great-v [2 3 9 11 4 4 8 11 11 11 3 3 4 2 3]) 

(defn has-element?
  [el vect]
  "Returns true if vector contains element"
      (= (some #{el} vect) el)) 

(defn partial-at-point [n-point v]
  (vec (take (inc n-point) v))) 

(defn change-ace-value [v]
  (let [point-17 (find-n-point v 17)
        partial-v (partial-at-point point-17 v)
        index-of-11 (.indexOf partial-v 11)]
    (if (and (> (reduce + partial-v) 21)
             (not= index-of-11 -1))
      (recur (vec
               (concat (assoc partial-v index-of-11 1)
                       (drop (inc point-17) v))))
      partial-v))) 

(change-ace-value great-v) 
  
;;;;;;  if sum-of partial-v > 21
;;;;;;    return remove-11 partial-v
;;;;;;  recur concat partial-v (dropped great-v)
;;;;;;  else
;;;;;;    return partial-v 
  
  (vec (concat partial-v (drop (inc point-17) great-v)))
) 

(defn change-ace-value [great-v]

  ) 


















;(vec (take (inc (find-n-point great-v 17)) great-v)) 
;
;(defn change-ace-val [v]
;  (let [tip-index (dec (count
;                   (filter #(<= % 21)
;                           (reductions + v))))]
;    (while (< (sum-until v tip-index) 21)
;      (recur (assoc v tip-index 1))
;
;(change-ace-val array) 
;
;(loop [i 0]
;  (when (< i 5)
;    (println i)
;    (recur (inc i)))) 
;
;(defn find-n-point [v n]
;    "Index of the number which stands
;     where the sum up until then is over 21." 
;    (count (filter #(<= % n)
;            (reductions + v)))) 
