(ns luckyseven.core
  (:require [cheshire.core :refer :all]
            [clojure.data.json :as json])
  (:gen-class)) 

(def random-seed (json/read-str (slurp "seed/gull.json")
                                                :key-fn keyword)) 

(defn fetch-seed []
  ((random-seed :array) (rand-int (count (random-seed :array))))) 

(defn random-int [max]
    (let [r (java.util.Random. (fetch-seed))]
      (.nextInt r max))) 

(def symbols ["bigwin", "blank", "coin", "blank", "bar1", "blank" "bar2", "blank", "bar3", "blank", "lucky7", "blank"])

(def win-symbols [(symbols (random-int 7)) (symbols (random-int 7)) (symbols (random-int 7))]) 

(defn check-bar
  [] 
  "Appropriates returns by symbol sequence"
  (if (= "bar1" (win-symbols 0) (win-symbols 1) (win-symbols 2)) 10
    (if (= "bar2" (win-symbols 0) (win-symbols 1) (win-symbols 2)) 20
      (if (= "bar3" (win-symbols 0) (win-symbols 1) (win-symbols 2)) 30 2)))) 

(defn count-sym
  "counts similar symbols in vector
  and returns it decremented (unless zero) to be used as index"
  [symb]
  (let [counted (count (filter (fn [x] (.contains x symb)) win-symbols))]
    counted)) 

(def result-vector
  "Maps return multiplier for number of symbol instances. 3 coins give 100 etc."
  [:coin ([0 1 3 100] (count-sym "coi")) 
   :blank ([0 0 0 0] (count-sym "bla")) 
   :bigwin ([0 0 0 1000] (count-sym "big")) 
   :bar ([0 0 0 '(check-bar)] (count-sym "bar")) 
   :luckyseven ([0 0 0 500] (count-sym "luc"))]) 

(def multipl (reduce + (filter number? (eval result-vector)))) 

(defn -main
  "Input: betsize. Output: winnings symbols and total returns
   Example: [['luckyseven' 'luckyseven' 'bigwin'], 0]"
  [betsize]
  (let [betsize (read-string betsize)]
    (println (generate-string [win-symbols (* multipl betsize)])))) 

