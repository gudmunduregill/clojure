(ns random-name.core
  (require [clojure.string :as str])
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]

;; Random names - version beta

(require '[clojure.string :as str])

  (defn do-letter[letter n]
    "lists a letter n many times"
    (repeat n letter))

  (def consonants
    (vec (flatten
    [(do-letter "t" 9)(do-letter "n" 7)(do-letter "s" 6)(do-letter "h" 6)(do-letter "r" 6)
     (do-letter "d" 4)(do-letter "l" 4)(do-letter "c" 3)(do-letter "n" 3)(do-letter "m" 2)
     (do-letter "w" 2)(do-letter "f" 2)(do-letter "g" 2)(do-letter "y" 2)(do-letter "p" 2)
     (do-letter "b" 2) "v" "k"
     
     "ch" "th" "ph" "gl" "tr"])))

 (def vowels
   (vec (flatten [(do-letter "e" 13)(do-letter "a" 8)(do-letter "i" 8)(do-letter "o" 7)
                   "ou" "ey" "au" "ai" "ee" "oo" "ia" "oi"])))

 ;; A vowel and a consonant together make a syllable

 (defn make-syllable[]
   (clojure.string/join  [(consonants (rand-int (count consonants)))
    (vowels (rand-int (count vowels)))]))


 (defn make-name[n]
   (clojure.string/capitalize (clojure.string/join (repeatedly (/ n 2) #(make-syllable)))))

(println (make-name 6)))

