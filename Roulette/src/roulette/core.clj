(ns roulette.core
  (:require [clojure.data.json :as json])
  (:gen-class)) 

(def random-seed (json/read-str (slurp "seed/gull.json")
                                                :key-fn keyword)) 

(defn fetch-seed []
  ((random-seed :array) (rand-int (count (random-seed :array))))) 

(defn random-int [max]
    (let [r (java.util.Random. (fetch-seed))]
      (.nextInt r max))) 

(defn -main
  "Input: '[[wager, [numbers]]...]', type JSON/String. 
  Where wager will be an integer and numbers will become vector of integers.
  Output: '[returns, lucky-number]', type JSON/String.
  Where returns is total sum of the returns on player winnings and lucky-number
  is the great integer itself."
  [bet]

  (def number (random-int 38)) 

  (defn has-element?
    [el vect]
    "Returns true if vector contains element."
      (= (some #{el} vect) el))  

  (defn calc-returns
    "Calculates returns on wager. 
     (wager * ⌊(35 / k)⌋)"
     [v wager]
     (+ wager (* wager (Math/floor (/ 35 (count v))))))

  (def total-winnings
    "Vectors containing the lucky number."
    (reduce + (filter number?
      (for [i (json/read-str bet)]
          (let [wager (first i)
                numbers (second i)]
            (if (true? (has-element? number numbers))

              (calc-returns numbers wager))))))) 

  (println (json/write-str [number, total-winnings])))  
