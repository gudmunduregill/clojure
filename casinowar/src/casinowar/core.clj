(ns casinowar.core
  (:require [cheshire.core :refer :all]
            [clojure.data.json :as json])
  (:gen-class)) 

(def random-seed (json/read-str (slurp "seed/gull.json")
                                :key-fn keyword)) 

(defn fetch-seed []
  ((random-seed :array) (rand-int (count (random-seed :array))))) 

(defn random-int [max]
  (let [r (java.util.Random. (fetch-seed))]
          (.nextInt r max))) 

(defn shuffle-with-seed
  "Return a random permutation of coll with a seed"
  [coll seed]
  (let [al (java.util.ArrayList. coll)
        rnd (java.util.Random. seed)]
    (java.util.Collections/shuffle al rnd)
    (clojure.lang.RT/vector (.toArray al)))) 

(defn make-decks
  "Returns n decks of cards for every bet."
  [n]
  (vec (shuffle-with-seed (take (* n 52) (cycle (for [i ["♤" "♡" "♢" "♧"]
        j (range 2 15)]
    (do [i j])))) (fetch-seed)))) 

(defn first-round
  "Returns first and second cards in deck as player and dealer are dealt."
  [all-cards]
  [(first all-cards) (second all-cards)])

(defn second-round
  "Third and Fourth. This happens if player and dealer draw in first round."
  [all-cards]
  [(all-cards 2) (all-cards 3)])

(defn return-integers
  "Takes a mixed collection and returns a vector of its integers."
  [mixed-collection]
  (vec (filter number? (flatten mixed-collection))))

(defn find-winner
  "Evalutes two cards and returns winner. First card is assumed to be player's."
  [points]
  (let [player-pts (points 0)
        dealer-pts (points 1)]
  (if (= player-pts dealer-pts)
    "draw"
    (if (> player-pts dealer-pts)
      "player" "dealer"))))

(def result-map
  "Maps all actions-to-results for the server to command payments and UI animations."
  (let [cards (make-decks 6)]
    {:first-round (first-round cards) :first-winner (find-winner (return-integers (first-round cards)))
     :second-round (second-round cards) :second-winner (find-winner (return-integers (second-round cards)))}))

;; Input: wager
;; Stdout: [[data], winner, returns]
(defn -main
    "Casino War"
    [& args]
     (println (generate-string result-map)))
